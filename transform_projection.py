import cartopy.crs as ccrs
import numpy as np
from lxml import etree
import json
import csv
from collections import defaultdict

FSA_boundaries = defaultdict(list)
polygon = []
pos_list = ''
for event, elem in etree.iterparse('lfsa000b16g_e.gml', events=('end',)):
    if elem.tag.endswith('CFSAUID'):
        FSA = elem.text
    if elem.tag.endswith('PolygonPatch'):
        FSA_boundaries[FSA].append(polygon)
        polygons=[]
    if elem.tag.endwith('exterior'):
        polygon.append({'outerBoundaryIs':pos_list})
    if elem.tag.endwith('interior'):
        polygon.append({'innerBoundaryIs':pos_list})
    if elem.tag.endswith('posList'):
        pos_list = elem.text

    elem.clear()
    while elem.getprevious() is not None:
        del elem.getparent()[0]

dest_crs = ccrs.PlateCarree()
source_crs = ccrs.LambertConformal(
                    central_longitude=-91.866667,
                    central_latitude=63.390675,
                    false_easting=6200000.0,
                    false_northing=3000000.0,
                    standard_parallels=(49., 77.))

with open('fsa_boundaries.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(['FSA','PosList'])

    for k, polygons in FSA_boundaries.items():
        new_polys = []
        for polygon in polygons:
            new_poly = []
            for linearring in polygon:
                new_lr = {}
                for boundarytype in linearring.keys():
                    pos = list(map(float, linearring[boundarytype].split(' ')))
                    x_pos = np.array([pos[i] for i in range(len(pos)) if i%2==0])
                    y_pos = np.array([pos[i] for i in range(len(pos)) if i%2==1])
                    new_pos = dest_crs.transform_points(source_crs, x_pos, y_pos)
                    new_pos = [[new_pos[i,0], new_pos[i,1]] for i in range(new_pos[:,:2].shape[0])]
                    new_lr = {boundarytype:new_pos}
            new_poly.append(new_lr)
        new_polys.append(new_poly)
        new_polys_json = json.dumps(new_polys)

        csv_writer.writerow([k, new_polys_json])


